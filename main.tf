terraform {
  backend "gcs" {
    bucket  = "kranthi-tfstate"
    prefix  = "terraform/state"
  }
}

resource "google_storage_bucket" "auto-expire" {
  name          = "kranthi26a1-public-access-bucket1"
  project       = "kranthi-kumar26a1"
  location      = "US"
  force_destroy = true
  public_access_prevention = "enforced"
}